<?php

namespace App\Policies;

use App\User;
use App\Book;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

        /**
     * Determine if the given book can be checkout by the user.
     *
     * @param User $user
     * @param Book $book
     * @return bool
     */
    public function checkout(User $user, Book $book)
    {
      return  !$book->canCheckout($user);
    }
}
