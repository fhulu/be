<?php
namespace App\Imports;

use Exception;
use Generator;
use SplFileObject;

class FromCsv
{
    /**
     * new file
     * @var SplFileObject
     */
    protected $file;

    /**
     * FromCsv constructor.
     * @param $file_name
     * @param string $open_mode
     * @throws Exception
     */
    public function __construct($file_name, $open_mode = 'r')
    {
        if (!$this->exists($file_name)){
            throw new Exception("File does not exist at path {$file_name}");
        }
        $this->file = new SplFileObject($file_name, $open_mode);
    }

    /**
     * avoid exceed a memory limit with Generator on large file
     * @return Generator
     */
    public function iteratorGenerator()
    {
        while (!$this->file->eof()) {
            yield $this->file->fgetcsv();
        }
    }

    /**
     * Determine if a file  exists.
     *
     * @param  string  $path
     * @return bool
     */
    public function exists($path)
    {
        if (stream_is_local($path)){
            return file_exists($path);
        }
        $handle = @fopen($path, 'r');
        return ($handle) ? true : false;
    }

    /**
     * Extract the file extension from a file path.
     *
     * @param  string  $path
     * @return string
     */
    public function extension($path)
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }
}