<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserBookResource;
use App\User;

class UserBookController extends Controller
{
    public function books(Request $request, User $user)
    {
        if (!$this->isValidUser($user)){
            abort(403, "This action is unauthorized.");
        }
        if ($request->has('due') && (int) $request->query('due') === 1) {
            $books = $user->load(['books' => function ($query) {
                $query->where('due_at', '>', now());
            }]);
            return  $books->books;
        }
        return UserBookResource::collection( auth()->user()->books);
    }

    public function book(User $user, Book $book)
    {
        if (!$this->isValidUser($user)){
            abort(403, "This action is unauthorized.");
        }
        $bk = $book->load(['users' => function ($query) use($user) {
            $query->where('user_id', $user->id);
        }]);

        return new UserBooksResource($bk);
    }

    private function isValidUser(User $user): bool
    {
        return $user->id === auth()->user()->id;
    }
}
