<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Resources\BookResource;
use App\Http\Resources\BooksResource;


class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['show']]);
    }

    public function index()
    {
        return BooksResource::collection(Book::paginate(10));
    }

    public function show(Book $book)
    {
        return new BookResource($book);
    }

    public function checkout(Request $request,Book $book)
    {
        $this->authorize('checkout',  $book);
        return app()->LibraryService->checkout($request->user(), $book);
    }

    public function return(Request $request, Book $book)
    {
        return app()->LibraryService->return($request->user(), $book);
    }

}
