<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function show()
    {
        return new UserResource(auth()->user()); 
    }

    public function update(Request $request)
    {
        $user = auth()->user()->update($request->all());
        return new UserResource(auth()->user()); 
    }
}
