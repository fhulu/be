<?php
namespace App\Services;

use App\Book;
use App\Policies\BookPolicy;
use App\User;
use App\Http\Resources\UserBooksResource;

class LibraryService
{
    public function checkout (User $user, Book $book)
    {

        //  if (!(new BookPolicy())->checkout($user,$book)) {
        //        abort(403, "This action is unauthorized.");
        //    }

        $book->users()->save($user, ['due_at' => now()->addWeeks(3), 'return_at' => null]);

        return new UserBooksResource($this->response($user,$book));
   }

    public function return(User $user, Book $book)
    {
        $book->users()->sync([$user->id => ['return_at' => now()]]);

        return new UserBooksResource($this->response($user,$book));
    }

    private function response(User $user, Book $book)
    {
        return $book->load(['users' => function ($query) use($user) {
            $query->where('user_id', $user->id);
        }]);
    }


}