# fhulufhelo-be

## ### Maybe this will help to quckly navigate through my project

### Environment
I use Homestead but i also  set Index Lengths & MySQL / MariaDB incase you are running a version of MySQL older than the 5.7.7 local. 

### let Roll

- Clone repo
- cd repo
- mv .env.example .env
- connect to mysql
- php artisan key:generate
- php artisan serve
- php artisan migrate


### Seeding data

Class "BookSeeder"

const `FILE_SRC`  hold file location
[ https://raw.githubusercontent.com/zygmuntz/goodbooks-10k/master/books.csv]( https://raw.githubusercontent.com/zygmuntz/goodbooks-10k/master/books.csv " https://raw.githubusercontent.com/zygmuntz/goodbooks-10k/master/books.csv")

So Command `php artisan db:seed` wil automatically insert data

##### Handle invalid seeding data
I fixed `publication_year` field and set default year to the current year for any invalid year number that can't be parsed and fix those that can be fixed

### API
All route define in routes/api

because some api need authentication, u can start by registering/login 

### Middleware
simple middleware  to “force”  app to return JSON, and and accept json
`App\Http\Middleware\AcceptJSON`


### Policies
`App\Policies\BookPolicy`

### LibarayService
`App\Services\LibraryService`

### Service Providers & Services
`App\Providers\BookServiceProvider`

`App\Services\LibraryService`