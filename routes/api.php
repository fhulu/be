<?php

use Illuminate\Http\Request;

// Authentication APIs for managing user access
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('register', 'AuthController@register');
});

// Books APIs for interacting with Library Books
Route::get('books', 'BookController@index');
Route::get('books/{book}', 'BookController@show');
Route::post('books/{book}/checkout', 'BookController@checkout');
Route::post('books/{book}/return', 'BookController@return');

// User APIs for managing user accounts
Route::get('user/{user}', 'UserController@show');
Route::put('user/{user}','UserController@update');

// User Books APIs for interacting with Library Books that have been checked out by a user
Route::get('user/{user}/books','UserBookController@books');
Route::get('user/{user}/books/{book}','UserBookController@book');
